# Hotmaps Graphical User Interface 
## Introduction Page (Animation)
![intro_gif][intro]

## Description
Once the toolbox is opened, the disclaiming page is shown ([Fig.1](#Fig1)). Besides the disclaimer message, additional information regarding the objectives of the Hotmaps toolbox as well as the link to the Hotmaps project website and the data repositories is provided.

<a name="Fig1">![disclaimer][Fig1] </a>
_Fig. 1 Short introduction to Hotmas toolbox_


The Hotmaps GUI is a GIS-based interface. By closing the disclaiming page, the user sees the map of Europe. By default, the heat demand density map of EU-28 countries and NUTS 3 layer are depicted. In addition to these two maps, some tools and buttons can be seen in GUI. These tools are illustrated in [Fig.2](#Fig2)

<a name="Fig2">![first_glance][Fig2] </a>
_Fig. 2 Initial Page_

Here you can spot at first glance 3 parts of the user interface:
1. a Toolbar at the top
2. the map itself
3. and some tools for changig the style of the map

In the follwing chapetrs we will have a look at each of this 3 parts:

## 1. Upper Toolbar
![first_glance][Fig3] 
### Short Description
With this toolbar you can:
1. [_Connect:_](#connect) register and login into the web application in order to save your work
2. [_Go To Place:_](#go-to-place) zoom in to a specific region by typing the name 
3. [_Layers:_](#layers) show the layer sidebar
4. [_Selection Tools_:](#selection-tool) enable or disable the selection tools
5. [_Show Result:_](#show-result) show the result sidebar of your selected regions
6. [_Feedback:_](#feedback) give us Feedback regarding the tool

### Long Description

#### Connect
##### Login
###### Animation
![login_gif][login_gif]
###### Description
After [registering](#register) and activating your account you should be able to login with your email and password (see [Fig.4](#Fig4) below).

<a name="Fig4">![login_png][login]</a>

_Fig.4.: Login Form_

##### Register
![register_gif][register_gif]
###### Description
Here you can create a account for the _hotmaps_ website. After submitting the [form](#Fig5) you will receive an email for actvating your account. With your account you will be able to save your progress.

<a name="Fig5">![register][register]</a>

_Fig.5.: RegisterForm_

##### Recover
![recover_gif][recover_gif]
###### Description
If you ever forget your password you can recover it under this menu (see [Fig.6.](#Fig6) below). Please be aware to set a new password afterwards. 

<a name="Fig6">![recover][recover]</a>

_Fig.6.: Recover Form_


#### Go To Place
You can zoom to a specifc region by typing its name (see animation below)
 
![go_to_place_gif][go_to_place]

#### Layers
![layers_gif][layers]
###### Description
By pressing this Button a sidebar with different kind of layers is shown at the left.
Following layers can be found and visualized:


#### Selection Tools
![selection_tools_gif][selection_tools]

#### Show Result

![show_result_gif][show_result]

#### Feedback
![feedback_gif][feedback]

## 2. Mapping
![maping_gif][mapping_gif]

### Description
![maping_gif][mapping]

[Fig1]: general_tool_functionalities_and_structure/disclaimer.png

[Fig2]: general_tool_functionalities_and_structure/gui_navigation.png

[intro]: general_tool_functionalities_and_structure/intro.gif

[Fig3]: general_tool_functionalities_and_structure/toolbar_up.png

[feedback]: general_tool_functionalities_and_structure/feedback.gif

[show_result]: general_tool_functionalities_and_structure/show_result.gif

[selection_tools]: general_tool_functionalities_and_structure/selection_tools.gif

[layers]: general_tool_functionalities_and_structure/layers.gif

[go_to_place]: general_tool_functionalities_and_structure/go_to_place.gif

[login_gif]: general_tool_functionalities_and_structure/login.gif

[register_gif]: general_tool_functionalities_and_structure/register.gif

[recover_gif]: general_tool_functionalities_and_structure/recover.gif

[mapping_gif]: general_tool_functionalities_and_structure/mapping.gif

[mapping]: general_tool_functionalities_and_structure/mapping.png

[login]: general_tool_functionalities_and_structure/login.png

[register]: general_tool_functionalities_and_structure/register.png

[recover]: general_tool_functionalities_and_structure/recover.png